<!--
 * Copyright (C) 2023 Eclipse Foundation and others. 
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-FileType: DOCUMENTATION
 * SPDX-FileCopyrightText: 2023 Eclipse Foundation
 * SPDX-License-Identifier: EPL-2.0
-->

# Security Policy

## Supported Versions

Security updates are applied to the HEAD of the master branch.

## Reporting a Vulnerability

Use the Eclipse Foundation Project Handbook's [Issue Tracker](https://gitlab.eclipse.org/eclipse/technology/dash/org.eclipse.dash.handbook/-/issues) to report a vulnerability.