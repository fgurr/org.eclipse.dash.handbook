////
 * Copyright (C) 2015 Eclipse Foundation, Inc. and others. 
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-FileType: SOURCE
 *
 * SPDX-FileCopyrightText: 2015 Eclipse Foundation, Inc.
 * SPDX-FileCopyrightText: 2015 Contributors to the Eclipse Foundation
 *
 * SPDX-License-Identifier: EPL-2.0
////

[[preamble]]
== Overview

This document provides you with the information that you need to create a new {forgeName} open source project or become a committer on an existing one.

The _{edpLink}_ (EDP) is the foundational document for {forgeName} projects and committers. It describes the manner in which we do open source software. The EDP does not prescribe any particular development methodology; it is more concerned with the larger-scale aspects of open source project life cycle, including such things as reviews, processes for running votes and elections, bringing new committers onto a project, etc. This document elaborates on some key points of the EDP.

[[preamble-principles]]
=== Principles

The _Open Source Rules of Engagement_ lie at the heart of the EDP:

Open :: Eclipse is open to all; Eclipse provides the same opportunity to all. Everyone participates with the same rules; there are no rules to exclude any potential Contributors which include, of course, direct competitors in the marketplace.

Transparent :: Project discussions, minutes, deliberations, Project plans, plans for new features, and other artifacts are open, public, and easily accessible.

Meritocratic :: Eclipse is a meritocracy. The more you contribute the more responsibility you will earn. Leadership roles in Eclipse are also merit-based and earned by peer acclaim.

[NOTE]
====
Employment status has no bearing at whether or not somebody can participate in an open source project at {forgeName}. Employment does not guarantee committer status; committer status must be earned by everybody. Committers and project leads are added to a project via xref:elections[election].
====

The Open Source Rules of Engagement define a foundation for _vendor neutral_ open source development. _Vendor-neutrality_ is concerned with maintaining a level playing field that is open to all comers: no vendor is permitted to dominate a project, and nobody can be excluded from participating in a project based on their employment status.

Quality and intellectual property cleanliness are also important principles.
 
_Quality_ means extensible frameworks and exemplary tools developed in an open, inclusive, and predictable process involving the entire community. From the consumption perspective, {forgeName} quality means good for users (exemplary tools are cool/compelling to use, indicative of what is possible) and ready for use by adopters. From the creation perspective, {forgeName} quality means working with a transparent and open process, open and welcoming to participation from technical leaders, regardless of affiliation.

_<<ip,Intellectual property>>_ (IP) is any artifact that is made available from a {forgeName} server (this includes source code management systems, the website, and the downloads server). Artifacts include (but are not limited to) such things as source code, images, XML and configuration files, documentation, and more. Strict rules govern the way that we manage IP and your responsibilities as a committer.

Code produced by an {forgeName} project is used by organisations to build products. These adopters of {forgeName} technology need to have some assurance that the IP they're basing their products on is _clean_: the organisation or individuals who claim copyright of the code are the legitimate copyright holders, and the copyright holders legitimately agree to make the code available under the license(s) that the project works under. As a committer, you must be careful that you do not copy code and inadvertently claim it as your own.
