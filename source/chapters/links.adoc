////
 * Copyright (C) 2015 Eclipse Foundation, Inc. and others. 
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-FileType: SOURCE
 *
 * SPDX-FileCopyrightText: 2015 Eclipse Foundation, Inc.
 * SPDX-FileCopyrightText: 2015 Contributors to the Eclipse Foundation
 *
 * SPDX-License-Identifier: EPL-2.0
////

[[links]]
== Links

Here are some other links that should help you with the process:

* link:Development Resources/HOWTO/Project Naming Policy[Project Naming Policy] - Selecting a name for your project;
* link:Development Resources/HOWTO/Pre-Proposal Phase[ Pre-Proposal
Phase] - The whole process for proposing a project;
** link:#Proposal_Creation_Links[Create a new proposal] on one of the
Eclipse Foundation Forges;
** Examples: http://eclipse.org/proposals/technology.aether/[Aether],
http://eclipse.org/proposals/mpc/[Marketplace Client]
* link:Architecture Council[Architecture Council] - The role of the
Architecture Council in a new project;
* xref:release-creation[Creation Reviews] -
Scheduling the proposal's creation review; and
* xref:incubation[Incubation Phase] -
After the project has been created.
* link:Community_Development_for_Eclipse_Projects[Community
Development] - Communities don't develop themselves! Some of the things
you need to think about.
